from flask import flash, Blueprint, render_template, render_template_string, redirect, url_for
from flask_plugins import get_all_plugins, get_enabled_plugins, get_plugin, Plugin, emit_event, connect_event, get_plugin_from_all
from flask_login import current_user, login_required
from app import AppPlugin, plugin_manager
from app.core.auth.decorators import login_admin_only
import os, signal
__plugin__ = "AdminPlugins"
__version__ = "1.0.0"


def flash_info_disable():
    flash("You can disable theses messages ... probably ... later in a config file")


def inject_index_description():
    return "<p> You can enable or disable plugins in the plugins menus. </p>"


def inject_navigation_link():
    return render_template_string(
        """
            <li><a href="{{ url_for('plugins.index') }}"><span class="fa fa-puzzle-piece"></span> Plugins</a></li>
        """)


bp = Blueprint("plugins", __name__, template_folder="templates")


@bp.route("/")
def index():
    return render_template("admin/plugins.html", plugins=get_all_plugins())

@bp.route('/restart-server/')
@login_required
@login_admin_only
def restart_server():
  os.kill(os.getpid(), signal.SIGHUP)
  return redirect(url_for("index"))
  

@bp.route("/disable/<plugin>")
@login_required
@login_admin_only
def disable(plugin):
    plugin = get_plugin(plugin)
    plugin_manager.disable_plugins([plugin])
    flash("Plugin {} is now disabled. You might need to wait for the next server reset for changes to take effect</a>.".format(__plugin__), "success")
    return redirect(url_for("index"))


@bp.route("/enable/<plugin>")
@login_required
@login_admin_only
def enable(plugin):
    plugin = get_plugin_from_all(plugin)
    plugin_manager.enable_plugins([plugin])
    flash("Plugin {} is now enabled ! You might need to wait for the next server reset for changes to take effect.".format(__plugin__), "success")
    return redirect(url_for("index"))


class AdminPlugins(AppPlugin):

    def setup(self):
        self.register_blueprint(bp, url_prefix="/admin/plugins/")

        connect_event("index_after_content", inject_index_description)
        connect_event("public_navigation_admin", inject_navigation_link)
